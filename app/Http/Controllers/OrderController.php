<?php

namespace App\Http\Controllers;

use App\DataTables\OrderDataTable;
use App\Http\Requests\OrderRequest;
use App\Order;
use App\Product;

class OrderController extends Controller
{
    public function index(OrderDataTable $datatable)
    {
        return $datatable->render('order.index');
    }

    public function edit(Order $order)
    {
        $product = Product::find($order->product_id);
        return view('cart', ['order' => $order, 'product' => $product]);
    }

    public function update(OrderRequest $request, Order $order)
    {
        $order->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'quantity' => $request->quantity
        ]);

        return redirect('orders');
    }

    public function destroy(Order $order)
    {
        $order->delete();
        return redirect('orders');
    }
}
