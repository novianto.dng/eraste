<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        return view('page.index');
    }
    public function shop()
    {
        $product = Product::all();

        return view('page.shop')->with('products', $product);
    }

    public function cart(Product $product)
    {
        return view('cart')->with('product', $product);
    }

    public function buy(Product $product, OrderRequest $request)
    {

        $order = Order::create([
            "product_id" => $product->id,
            "quantity" => $request->quantity,
            "no_order" => Order::count() + 1000,
            "name" => $request->name,
            "address" => $request->address,
            "phone" => $request->phone
        ]);

        return view('bought')->with('order', $order);
    }
}
