<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Http\Requests\NewUserRequest;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(UserDataTable $datatable)
    {
        return $datatable->render('user.index');
    }

    public function create()
    {
        return view('user.form');
    }

    public function store(NewUserRequest $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return redirect('/users');
    }

    public function edit(User $user)
    {
        return view('user.form')->with('user', $user);
    }

    public function update(UserRequest $request, User $user)
    {
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        if ($request->password) {
            $user->update([
                'password' => bcrypt($request->password)
            ]);
        }

        return redirect('users');
    }

    public function destroy(user $user)
    {
        $user->delete();

        return redirect('users');
    }
}
