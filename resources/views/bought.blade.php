@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-6 col-xs-6">
        <p>Order No</p>
        <p>Product Name</p>
        <p>Qty</p>
        <p>Total</p>
    </div>
    <div class="col-md-6 col-xs-6 text-left">
        <p>{{number_format($order->no_order,0,","," ")}}</p>
        <p>{{$order->product->code.' - '.$order->product->name}}</p>
        <p>{{$order->quantity}} Pcs</p>
        <p>Rp {{number_format($order->quantity * $order->product->price)}}</p>
    </div>
</div>
@endsection