@extends('layouts.main')

@section('content')
<h3>Order Information</h3>
<p>{{$product->code}} - {{$product->name}}</p>
<p>Rp.{{number_format($product->price, 2)}}</p>


<h3>Customer Information</h3>
@if(!empty($order))
<form action="{{url('order').'/'.$order->id}}" method="POST">
    {{ method_field('PUT') }}
    @else
    <form action="{{url('buy').'/'.$product->id}}" method="POST">
        @endif
        {{csrf_field()}}
        <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
            <label>Name</label>
            <input type="text" class="form-control" name="name" value="{{!empty($order) ? $order->name : ''}}">
            @if($errors->has('name'))
            <span class="help-block">
                {{$errors->first('name')}}
            </span>
            @endif
        </div>

        <div class="form-group {{$errors->has('phone') ? 'has-error' : ''}}">
            <label>Phone </label>
            <input type="text" class="form-control" name="phone" value="{{!empty($order) ? $order->phone : ''}}">
            @if($errors->has('phone'))
            <span class="help-block">
                {{$errors->first('phone')}}
            </span>
            @endif
        </div>

        <div class="form-group {{$errors->has('quantity') ? 'has-error' : ''}}">
            <label>Quantity </label>
            <input type="number" class="form-control" name="quantity" value="{{!empty($order) ? $order->quantity : ''}}">
            @if($errors->has('quantity'))
            <span class="help-block">
                {{$errors->first('quantity')}}
            </span>
            @endif
        </div>


        <div class="form-group {{$errors->has('address') ? 'has-error' : ''}}">
            <label>Address </label>
            <textarea class="form-control" rows="6" name="address">{{!empty($order) ? $order->address : ''}}</textarea>
            @if($errors->has('address'))
            <span class="help-block">
                {{$errors->first('address')}}
            </span>
            @endif
        </div>


        <button type="submit" class="btn btn-block btn-success">Submit</button>
    </form>
    @endsection