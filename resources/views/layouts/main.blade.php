<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="{{ asset('js/jquery.js')}}"></script>
    <script src="{{ asset('js/datatable.js')}}"></script>
    <title>Document</title>
</head>

<body>

    @include('layouts.navbar')

    <div class="container">
        @yield('content')
    </div>

    @stack('scripts')
</body>

</html>