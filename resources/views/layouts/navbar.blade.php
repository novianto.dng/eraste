<nav class="navbar navbar-default">
    <div class="container">
        @if(Auth::check())
        <ul class="nav navbar-nav">
            <li class="{{request()->routeIs('product*') ? 'active' : '' }}"><a href="{{url('products')}}">Product</a></li>
            <li class="{{request()->routeIs('order*') ? 'active' : ''}}"><a href="{{url('orders')}}">Order</a></li>
            <li class="{{request()->routeIs('user*') ? 'active' : ''}}"><a href="{{url('users')}}">User</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li> <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
        </ul>
        @else
        <a class="navbar-brand" href="#">
            Eraste
        </a>
        <ul class="nav navbar-nav">
            <li><a href="/" class="{{request()->routeIs('home') ? 'active' : '' }}">Home</a></li>
            <li><a href="{{url('shop')}}" class="{{request()->routeIs('shop*') ? 'active' : '' }}">Shop</a></li>
        </ul>
        @endif

    </div>
</nav>