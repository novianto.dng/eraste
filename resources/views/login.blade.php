@extends('layout.main')

@section('content')

<form action="{{url('login')}}" method="POST">
    {{csrf_field()}}
    <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
        <label>Email</label>
        <input type="email" class="form-control" name="email">
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
        <label>Password </label>
        <input type="password" class="form-control" name="password">
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>


    <button type="submit" class="btn btn-block btn-success">Submit</button>
</form>

@endsection