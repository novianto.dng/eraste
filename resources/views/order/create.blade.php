@section('content')

<h3>Order Information</h3>

<h3>Customer Information</h3>

<form action="{{url('product')}}" method="POST">
    {{csrf_field()}}
    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
        <label>Name</label>
        <input type="text" class="form-control" name="name">
        @if($errors->has('name'))
        <span class="help-block">
            {{$errors->first('name')}}
        </span>
        @endif
    </div>

    <div class="form-group {{$errors->has('price') ? 'has-error' : ''}}">
        <label>Phone </label>
        <input type="text" class="form-control" name="phone">
        @if($errors->has('phone'))
        <span class="help-block">
            {{$errors->first('phone')}}
        </span>
        @endif
    </div>

    <div class="form-group {{$errors->has('address') ? 'has-error' : ''}}">
        <label>Address </label>
        <textarea class="form-control" rows="6" name="address"></textarea>
        @if($errors->has('address'))
        <span class="help-block">
            {{$errors->first('address')}}
        </span>
        @endif
    </div>


    <button type="submit" class="btn btn-block btn-success">Submit</button>
</form>

@endsection