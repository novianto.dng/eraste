@extends('layouts.main')

@section('content')

<h3>Products</h3>

@foreach($products->chunk(4) as $chunk )

<div class="row">
    @foreach($chunk as $item)
    <div class="col-xs-6 col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">

                <img src="{{asset('product.jpg')}}" alt="product" class="img-rounded img-responsive">

                <p>{{$item->name}}</p>

                <p>Rp. {{number_format($item->price, 2)}}</p>

                <a href="{{url('cart').'/'.$item->id}}" class="btn btn-block btn-success">Beli</a>
            </div>
        </div>

    </div>
    @endforeach
</div>

@endforeach

@endsection