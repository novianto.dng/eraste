@extends('layouts.main')

@section('content')
<h3>Create Form</h3>

@if(!empty($product))
<form action="{{url('product').'/'.$product->id}}" method="POST">
    {{ method_field('PUT') }}
    @else
    <form action="{{url('product')}}" method="POST">
        @endif
        {{csrf_field()}}
        <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
            <label for="nama">Name</label>
            <input type="text" class="form-control" name="name" {{!empty($product) ? "value=$product->name" :"" }}>
            @if($errors->has('name'))
            <span class="help-block">
                {{$errors->first('name')}}
            </span>
            @endif
        </div>

        <div class="form-group {{$errors->has('price') ? 'has-error' : ''}}">
            <label for="nama">Price </label>
            <input type="text" class="form-control" name="price" {{!empty($product) ? "value=$product->price" : ""}}>
            @if($errors->has('price'))
            <span class="help-block">
                {{$errors->first('price')}}
            </span>
            @endif
        </div>


        <button type="submit" class="btn btn-block btn-success">Submit</button>
    </form>
    @endsection