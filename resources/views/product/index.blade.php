@extends('layouts.main')

@section('content')
<a href="{{url('product/create')}}" class="btn btn-primary">Create Product</a>

{!! $dataTable->table() !!}

@endsection

@push('scripts')
{!! $dataTable->scripts() !!}
@endpush