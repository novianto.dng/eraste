@extends('layouts.main')

@section('content')

@if(!empty($user))
<form action="{{url('user').'/'.$user->id}}" method="POST">
    {{ method_field('PUT') }}
    @else
    <form action="{{url('user')}}" method="POST">
        @endif
        {{csrf_field()}}
        <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
            <label>Name</label>
            <input type="text" class="form-control" name="name" {{!empty($user) ? "value=$user->name" :"" }}>
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
            <label>Email </label>
            <input type="email" class="form-control" name="email" {{!empty($user) ? "value=$user->email" : ""}}>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
            <label>Password </label>
            <input type="password" class="form-control" name="password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        @if(empty($user))
        <div class="form-group {{$errors->has('password_confirmation') ? 'has-error' : ''}}">
            <label>Password Confirm </label>
            <input type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

        </div>
        @endif


        <button type="submit" class="btn btn-block btn-success">Submit</button>
    </form>

    @endsection