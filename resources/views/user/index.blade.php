@extends('layouts.main')

@section('content')
<a href="{{url('user/create')}}" class="btn btn-primary">Create user</a>
{!! $dataTable->table() !!}

@endsection

@push('scripts')
{!! $dataTable->scripts() !!}
@endpush