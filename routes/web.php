<?php

use App\DataTables\ProductDataTable;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('home');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

Route::name('shop.')->group(function () {
    Route::get('shop', 'PageController@shop')->name('shop');
    Route::get('cart/{product}', 'PageController@cart')->name('cart');
    Route::post('buy/{product}', 'PageController@buy')->name('buy');
});

Route::group(['middleware' => ['auth']], function () {
    Route::name('product.')->group(function () {
        Route::get('products', 'ProductController@index')->name('index');
        Route::get('product/create', 'ProductController@create')->name('create');
        Route::post('product', 'ProductController@store')->name('store');
        Route::get('product/edit/{product}', 'ProductController@edit')->name('edit');
        Route::put('product/{product}', 'ProductController@update')->name('update');
        Route::get('product/delete/{product}', 'ProductController@destroy')->name('delete');
    });

    Route::name('order.')->group(function () {
        Route::get('orders', 'OrderController@index')->name('index');
        Route::get('order/edit/{order}', 'OrderController@edit')->name('edit');
        Route::put('order/{order}', 'OrderController@update')->name('update');
        Route::get('order/delete/{order}', 'OrderController@destroy')->name('delete');
    });

    Route::name('user.')->group(function () {
        Route::get('users', 'UserController@index')->name('index');
        Route::get('user/create', 'UserController@create')->name('create');
        Route::post('user', 'UserController@store')->name('store');
        Route::get('user/edit/{user}', 'UserController@edit')->name('edit');
        Route::put('user/{user}', 'UserController@update')->name('update');
        Route::delete('user/delete', 'UserController@destroy')->name('delete');
    });

    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
});

// Auth::routes();


// Route::get('/home', 'HomeController@index')->name('home');
